RiseupVPN
-----------------------

Anonymous VPN. Easy, fast, secure.

This repo has everything needed to build RiseupVPN on different platforms
(windows, mac, linux).

RiseupVPN is a branded build of Bitmask Lite, written in go.


Dependencies
------------------------

* golang
* make
* python (python3, for building scripts)

Dependencies (Windows)
------------------------

* nsis
* nssm

Building (Windows)
------------------------

make deps_win
make build_win
