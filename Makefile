SYSTRAY := 0xacab.org/leap/bitmask-systray
STAGING := staging

deps_win:
	choco install -y golang python upx nssm nsis
helper_win:
	go build -ldflags "-s -w" -o $(STAGING)/bitmask_helper.exe .\helper
	upx $(STAGING)/bitmask_helper.exe
systray_win:
	go get -u $(SYSTRAY)
	go build -tags "standalone" -ldflags "-H windowsgui -s -w" -o $(STAGING)/bitmask-systray.exe $(SYSTRAY)
build_win: helper_win systray_win
	echo "[+] building windows"
	if not exist dist mkdir dist
	make -C win
	"C:\Program Files (x86)\NSIS\makensis.exe" win/RiseupVPN-installer.nsi
build_osx:
	echo "[+] building osx"
build_snap:
	echo "[+] building snap"
