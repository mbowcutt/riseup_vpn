"""
generate.py

Generate a NSI installer for a given provider.
"""

from string import Template

from provider import *


TEMPLATE = 'template.nsi'
INSTALLER = APP_NAME + '-installer.nsi'


def get_files(which):
    files = "\n"
    if which == 'install':
        action = "File "
    elif which == 'uninstall':
        action = "Delete $INSTDIR\\"
    else:
        action = ""

    # TODO get relative path
    for item in open('payload/' + which).readlines():
        files += "  {action}{item}".format(
            action=action, item=item)
    return files


vardict = {
    'app_name': APP_NAME,
    'app_name_lower': APP_NAME.lower(),
    'url': URL,
    'extra_install_files': get_files('install'),
    'extra_uninstall_files': get_files('uninstall')
}


template = Template(open(TEMPLATE).read())
with open(INSTALLER, 'w') as output:
    output.write(template.safe_substitute(vardict))

print("[+] NSIS installer script written to {path}".format(path=INSTALLER))
